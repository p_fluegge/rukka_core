<?php
namespace Rukka\Core;

use Rukka\Core\Converter\HtmlConverter;
use Rukka\Core\Converter\PdfConverter;

class Pdf
{

    private $converter = [
        'html' => HtmlConverter::class,
        'pdf' => PdfConverter::class,
    ];

    private $pathToTemplate = __DIR__ . '/../src/views/template.php';
    private $outputPath = '/tmp/invoice';
    private $data = null;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function setTemplate($pathToTemplate)
    {
        return $this->pathToTemplate = $pathToTemplate;
    }

    public function setPath($outputPath)
    {
        return $this->outputPath = $outputPath;
    }

    public function generate($output = 'pdf')
    {
        $file = new $this->converter[$output]($this->data, $this->pathToTemplate, $this->outputPath . '.' . $output);

        return $file->generate();
    }

    public function output($output = 'pdf')
    {
        $file = new $this->converter[$output]($this->data, $this->pathToTemplate, $this->outputPath . '.' . $output);
        $file->output();
    }

}