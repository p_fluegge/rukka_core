<?php
namespace Rukka\Core\Converter;

interface ConverterInterface
{
    public function generate(); //Ausgabe im Browser
    public function output(); //Ausgabe als Stream
}