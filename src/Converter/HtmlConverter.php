<?php
namespace Rukka\Core\Converter;

class HtmlConverter implements ConverterInterface
{

    private $pathToTemplate;
    private $data;
    private $outputPath;

    public function __construct($data, $pathToTemplate, $outputPath)
    {
        $this->data = $data;
        $this->pathToTemplate = $pathToTemplate;
        $this->outputPath = $outputPath;
    }

    public function generate()
    {
        return $this->build();
    }

    public function output()
    {
        file_put_contents($this->outputPath, $this->generate());
    }

    private function build()
    {
        ob_start();
        $data = $this->data;
        include($this->pathToTemplate);
        $htmlFile = ob_get_contents();
        ob_end_clean();

        return $htmlFile;
    }

    public function format_array($input){
        echo '<pre>' . $input . '</pre>';
        exit;
    }
}