<?php
namespace Rukka\Core\Tests;

use Rukka\Core\Pdf;
use Rukka\Core\Test;
use Rukka\Core\Tests\TestCase;

class PdfTest extends TestCase
{
    /** @test */
    public function it_can_outputs_a_pdf_with_the_default_template_and_the_default_path()
    {
        $generator = new Pdf('Data');
        $generator->output('pdf');

        $this->assertFileExists('/tmp/invoice.pdf');
        unlink('/tmp/invoice.pdf');
    }

    /** @test */
    public function it_can_outputs_a_pdf_with_a_costum_template_and_the_default_path()
    {
        $generator = new Pdf('Data');
        $generator->setTemplate(__DIR__ . '/views/template_not_default.php');
        $generator->output('pdf');

        $this->assertFileExists('/tmp/invoice.pdf');
        unlink('/tmp/invoice.pdf');
    }

    /** @test */
    public function it_can_outputs_a_pdf_with_the_default_template_and_a_costum_path()
    {
        $generator = new Pdf('Data');
        $generator->setPath(__DIR__ . '/files/invoice');
        $generator->output('pdf');

        $this->assertFileExists(__DIR__ . '/files/invoice.pdf');
        unlink(__DIR__ . '/files/invoice.pdf');
    }

    /** @test */
    public function it_can_outputs_a_pdf_with_a_costum_template_and_a_costum_path()
    {
        $generator = new Pdf('Data');
        $generator->setTemplate(__DIR__ . '/views/template_not_default.php');
        $generator->setPath(__DIR__ . '/files/invoice');
        $generator->output('pdf');

        $this->assertFileExists(__DIR__ . '/files/invoice.pdf');
        unlink(__DIR__ . '/files/invoice.pdf');
    }

    /** @test */
    public function it_can_outputs_a_html_with_the_default_template_and_the_default_path()
    {
        $generator = new Pdf('Data');
        $generator->output('html');

        $this->assertFileExists('/tmp/invoice.html');
        unlink('/tmp/invoice.html');
    }

    /** @test */
    public function it_can_outputs_a_html_with_a_costum_template_and_the_default_path()
    {
        $generator = new Pdf('Data');
        $generator->setTemplate(__DIR__ . '/views/template_not_default.php');
        $generator->output('html');

        $this->assertFileExists('/tmp/invoice.html');
        unlink('/tmp/invoice.html');
    }

    /** @test */
    public function it_can_outputs_a_html_with_the_default_template_and_a_costum_path()
    {
        $generator = new Pdf('Data');
        $generator->setPath(__DIR__ . '/files/invoice');
        $generator->output('html');

        $this->assertFileExists(__DIR__ . '/files/invoice.html');
        unlink(__DIR__ . '/files/invoice.html');
    }

    /** @test */
    public function it_can_outputs_a_html_with_a_costum_template_and_a_costum_path()
    {
        $generator = new Pdf('Data');
        $generator->setTemplate(__DIR__ . '/views/template_not_default.php');
        $generator->setPath(__DIR__ . '/files/invoice');
        $generator->output('html');

        $this->assertFileExists(__DIR__ . '/files/invoice.html');
        unlink(__DIR__ . '/files/invoice.html');
    }

}
